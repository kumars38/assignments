def are_valid_groups(sNums, groups):
    #every student number must occur exactly once
    s = sorted(sNums)
    for i in range(len(s) - 1):
        if s[i] == s[i+1]:
            return False

    #every group must have 2-3 members
    for group in groups:
        if len(group) < 2 or len(group) > 3:
            return False
    return True